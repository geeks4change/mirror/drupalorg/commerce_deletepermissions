<?php
/**
 * @file commerce_deletepermissions.api.php
 */

/**
 * Announce entity types to create delete permissions for.
 *
 * @return array
 *   An array of entity types.
 */
function hook_commerce_deletepermissions_entity_types() {
  return array('commerce_order');
}
